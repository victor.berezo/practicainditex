package com.lab.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.lab.domain.Price;
import com.lab.domain.PriceOut;
import com.lab.infrastructure.h2.ServiceDB;
import com.lab.infrastructure.h2.dto.PriceRequestDBDto;
import com.lab.infrastructure.h2.dto.PriceResponseDBDto;
import com.lab.infrastructure.h2.map.PriceDBMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PriceUseCaseTest {

  @Mock
  private ServiceDB serviceDB;

  @Mock
  private PriceDBMapper priceDBMapper;

  private PriceUseCase priceUseCase;

  Price price = PricesObjectTest.getPrice();

  PriceRequestDBDto priceRequestDBDto = PricesObjectTest.getPriceRequestDBDto();

  @BeforeEach
  public void initEach(){
    priceUseCase = new PriceUseCase(priceDBMapper, serviceDB);
  }

  @Test
  void priceTest() {

    when(priceDBMapper.toRepository(price)).thenReturn(priceRequestDBDto);

    PriceResponseDBDto priceResponseDBDto = PricesObjectTest.getPriceResponseDBDto();
    when(serviceDB.findPrice(priceRequestDBDto)).thenReturn(priceResponseDBDto);

    PriceOut priceOutExpected = PricesObjectTest.getPriceOut();
    when(priceDBMapper.toDomain(priceResponseDBDto)).thenReturn(priceOutExpected);

    PriceOut priceOut = priceUseCase.price(price);

    assertEquals(priceOut, priceOutExpected);

  }

  @Test
  void priceTestNull() {

    when(priceDBMapper.toRepository(price)).thenReturn(priceRequestDBDto);

    PriceResponseDBDto priceResponseDBDto = PricesObjectTest.getPriceResponseDBDto();
    when(serviceDB.findPrice(priceRequestDBDto)).thenReturn(priceResponseDBDto);

    when(priceDBMapper.toDomain(priceResponseDBDto)).thenReturn(null);

    PriceOut priceOut = priceUseCase.price(price);

    assertEquals(priceOut, null);

  }
}
