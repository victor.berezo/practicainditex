package com.lab.application;

import com.lab.domain.Price;
import com.lab.domain.PriceOut;
import com.lab.infrastructure.h2.ServiceDB;
import com.lab.infrastructure.h2.map.PriceDBMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class PriceUseCase {

    private final PriceDBMapper priceDBMapper;

    private final ServiceDB serviceDB;

    public PriceOut price(Price price){
         return Optional.ofNullable(price)
                  .map(priceDBMapper::toRepository)
                  .map(serviceDB::findPrice)
                  .map(priceDBMapper::toDomain)
                 .orElse(null);
    }
}
