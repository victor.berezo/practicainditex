package com.lab.infrastructure.rest.map;

import com.lab.domain.Price;
import com.lab.domain.PriceOut;
import com.lab.infrastructure.rest.dto.PriceRequestDto;
import com.lab.infrastructure.rest.dto.PriceResponseDto;


public interface PriceMapper {

    Price toDomain(PriceRequestDto priceRequestDto);
    PriceResponseDto toInfrastructure(PriceOut priceOut);
}
