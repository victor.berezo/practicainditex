package com.lab.infrastructure.rest.map;

import com.lab.domain.Price;
import com.lab.domain.PriceOut;
import com.lab.infrastructure.rest.dto.PriceRequestDto;
import com.lab.infrastructure.rest.dto.PriceResponseDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class PriceMapperImpl implements PriceMapper {


    @Override
    public Price toDomain(PriceRequestDto priceRequestDto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(priceRequestDto,Price.class);
    }

    @Override
    public PriceResponseDto toInfrastructure(PriceOut priceOut) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(priceOut,PriceResponseDto.class);
    }
}
