package com.lab.infrastructure.h2.map;

import com.lab.domain.Price;
import com.lab.domain.PriceOut;
import com.lab.infrastructure.h2.dto.PriceRequestDBDto;
import com.lab.infrastructure.h2.dto.PriceResponseDBDto;

public interface PriceDBMapper {

    PriceRequestDBDto toRepository(Price price);
    PriceOut toDomain(PriceResponseDBDto priceResponseDBDto);
}
