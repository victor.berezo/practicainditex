package com.lab.infrastructure.h2.map;

import com.lab.infrastructure.h2.dto.PriceRequestDBDto;
import com.lab.infrastructure.h2.dto.PriceResponseDBDto;
import com.lab.infrastructure.h2.entity.Prices;

public interface PriceDBEntityMapper {

    Prices toInfrastructure(PriceRequestDBDto priceRequestDBDto);
    PriceResponseDBDto toDomain(Prices priceEntity);
}
