package com.lab.infrastructure.h2.map;

import com.lab.domain.Price;
import com.lab.domain.PriceOut;
import com.lab.infrastructure.h2.dto.PriceRequestDBDto;
import com.lab.infrastructure.h2.dto.PriceResponseDBDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class PriceDBMapperImpl implements PriceDBMapper{

    @Override
    public PriceRequestDBDto toRepository(Price price) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(price, PriceRequestDBDto.class);
    }

    @Override
    public PriceOut toDomain(PriceResponseDBDto priceResponseDBDto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(priceResponseDBDto,PriceOut.class);
    }
}
