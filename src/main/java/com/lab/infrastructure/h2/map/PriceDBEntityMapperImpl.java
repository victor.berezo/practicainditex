package com.lab.infrastructure.h2.map;

import com.lab.infrastructure.h2.dto.PriceRequestDBDto;
import com.lab.infrastructure.h2.dto.PriceResponseDBDto;
import com.lab.infrastructure.h2.entity.Prices;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class PriceDBEntityMapperImpl implements PriceDBEntityMapper{
    @Override
    public Prices toInfrastructure(PriceRequestDBDto priceRequestDBDto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(priceRequestDBDto, Prices.class);
    }

    @Override
    public PriceResponseDBDto toDomain(Prices priceEntity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(priceEntity,PriceResponseDBDto.class);
    }
}
