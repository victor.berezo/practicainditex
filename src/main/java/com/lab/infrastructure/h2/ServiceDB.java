package com.lab.infrastructure.h2;

import com.lab.infrastructure.h2.dto.PriceRequestDBDto;
import com.lab.infrastructure.h2.dto.PriceResponseDBDto;
import com.lab.infrastructure.h2.entity.Prices;
import com.lab.infrastructure.h2.map.PriceDBEntityMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ServiceDB {

    private final PriceDBEntityMapper priceDBEntityMapper;
    private final PriceRepository priceRepository;

    public PriceResponseDBDto findPrice(PriceRequestDBDto priceRequestDBDto){
        return Optional.ofNullable(priceRequestDBDto)
                .map(a-> priceRepository.price(priceRequestDBDto.getProductId(),priceRequestDBDto.getBrandId(),priceRequestDBDto.getApplicationDate()))
                .map(this::mapQuery)
                .map(priceDBEntityMapper::toDomain)
                .orElse(null);
    }

    private Prices mapQuery(List<Prices> prices){

        return prices.size()>0?prices.get(0):null;
    }
}
